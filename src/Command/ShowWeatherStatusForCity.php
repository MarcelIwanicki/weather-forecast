<?php

namespace App\Command;

use App\Service\WeatherUtil;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ShowWeatherStatusForCity extends Command
{
    protected static $defaultName = 'app:show-weather-by-city';
    private WeatherUtil $weatherUtil;

    /**
     * ShowWeatherStatusForIdCommand constructor.
     * @param WeatherUtil $weatherUtil
     */
    public function __construct(WeatherUtil $weatherUtil)
    {
        $this->weatherUtil = $weatherUtil;
        parent::__construct();
    }


    protected function configure(): void
    {
        $this->addArgument('city_name', InputArgument::REQUIRED, 'City name');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $city_name = $input->getArgument('city_name');
        $weather_status = $this->weatherUtil->getWeatherStatusByCityName($city_name);

        $output->writeln($weather_status);
        return 0;
    }
}