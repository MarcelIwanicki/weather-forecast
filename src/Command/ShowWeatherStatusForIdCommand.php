<?php

namespace App\Command;

use App\Service\WeatherUtil;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ShowWeatherStatusForIdCommand extends Command
{
    protected static $defaultName = 'app:show-weather';
    private WeatherUtil $weatherUtil;

    /**
     * ShowWeatherStatusForIdCommand constructor.
     * @param WeatherUtil $weatherUtil
     */
    public function __construct(WeatherUtil $weatherUtil)
    {
        $this->weatherUtil = $weatherUtil;
        parent::__construct();
    }


    protected function configure(): void
    {
        $this->addArgument('id', InputArgument::REQUIRED, 'Id of the weather_status');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $id = $input->getArgument('id');
        $weather_status = $this->weatherUtil->getWeatherStatusById($id);

        $output->writeln($weather_status);
        return 0;
    }
}