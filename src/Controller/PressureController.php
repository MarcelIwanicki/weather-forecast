<?php

namespace App\Controller;

use App\Entity\Pressure;
use App\Form\PressureType;
use App\Repository\PressureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/pressure")
 */
class PressureController extends AbstractController
{
    /**
     * @Route("/", name="pressure_index", methods={"GET"})
     */
    public function index(PressureRepository $pressureRepository): Response
    {
        return $this->render('pressure/index.html.twig', [
            'pressures' => $pressureRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="pressure_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $pressure = new Pressure();
        $form = $this->createForm(PressureType::class, $pressure, [
            'validation_groups' => 'new_pressure'
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($pressure);
            $entityManager->flush();

            return $this->redirectToRoute('pressure_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('pressure/new.html.twig', [
            'pressure' => $pressure,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="pressure_show", methods={"GET"})
     */
    public function show(Pressure $pressure): Response
    {
        return $this->render('pressure/show.html.twig', [
            'pressure' => $pressure,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="pressure_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Pressure $pressure): Response
    {
        $form = $this->createForm(PressureType::class, $pressure, [
            'validation_groups' => 'edit_pressure'
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('pressure_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('pressure/edit.html.twig', [
            'pressure' => $pressure,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="pressure_delete", methods={"POST"})
     */
    public function delete(Request $request, Pressure $pressure): Response
    {
        if ($this->isCsrfTokenValid('delete'.$pressure->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($pressure);
            $entityManager->flush();
        }

        return $this->redirectToRoute('pressure_index', [], Response::HTTP_SEE_OTHER);
    }
}
