<?php

namespace App\Controller;

use App\Entity\WeatherStatus;
use App\Form\WeatherStatusType;
use App\Repository\CityRepository;
use App\Repository\WeatherStatusRepository;
use App\Service\WeatherUtil;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/weather/status")
 */
class WeatherStatusController extends AbstractController
{

    /**
     * @Route("/", name="weather_status_index", methods={"GET"})
     */
    public function index(WeatherStatusRepository $weatherStatusRepository): Response
    {
        return $this->render('weather_status/index.html.twig', [
            'weather_statuses' => $weatherStatusRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="weather_status_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $weatherStatus = new WeatherStatus();
        $form = $this->createForm(WeatherStatusType::class, $weatherStatus, [
            'validation_groups' => 'new_weather_status'
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($weatherStatus);
            $entityManager->flush();

            return $this->redirectToRoute('weather_status_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('weather_status/new.html.twig', [
            'weather_status' => $weatherStatus,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="weather_status_show", methods={"GET"})
     */
    public function show(WeatherStatus $weatherStatus): Response
    {
        return $this->render('weather_status/show.html.twig', [
            'weather_status' => $weatherStatus,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="weather_status_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, WeatherStatus $weatherStatus): Response
    {
        $form = $this->createForm(WeatherStatusType::class, $weatherStatus, [
            'validation_groups' => 'edit_weather_status'
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('weather_status_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('weather_status/edit.html.twig', [
            'weather_status' => $weatherStatus,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="weather_status_delete", methods={"POST"})
     */
    public function delete(Request $request, WeatherStatus $weatherStatus): Response
    {
        if ($this->isCsrfTokenValid('delete' . $weatherStatus->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($weatherStatus);
            $entityManager->flush();
        }

        return $this->redirectToRoute('weather_status_index', [], Response::HTTP_SEE_OTHER);
    }


    public function cityAction($cityName, WeatherUtil $weatherUtil): Response
    {
        $weatherStatuses = $weatherUtil->getWeatherStatusByCityName($cityName);
        $city = $weatherUtil->getCityByName($cityName);

        return $this->render('weather_status/city.html.twig', [
            'location' => $city,
            'weatherStatuses' => $weatherStatuses,
        ]);
    }


    public function getCityByLocation($location, WeatherUtil $weatherUtil): Response
    {
        $weatherStatuses = $weatherUtil->getWeatherStatusByLocation($location);
        $city = $weatherUtil->getCityByLocation($location);

        return $this->render('weather_status/city.html.twig', [
            'location' => $city,
            'weatherStatuses' => $weatherStatuses,
        ]);
    }
}
