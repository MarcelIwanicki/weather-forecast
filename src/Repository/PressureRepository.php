<?php

namespace App\Repository;

use App\Entity\Pressure;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Pressure|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pressure|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pressure[]    findAll()
 * @method Pressure[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PressureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pressure::class);
    }

    // /**
    //  * @return Pressure[] Returns an array of Pressure objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Pressure
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
