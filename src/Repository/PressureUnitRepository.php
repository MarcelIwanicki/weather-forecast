<?php

namespace App\Repository;

use App\Entity\PressureUnit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PressureUnit|null find($id, $lockMode = null, $lockVersion = null)
 * @method PressureUnit|null findOneBy(array $criteria, array $orderBy = null)
 * @method PressureUnit[]    findAll()
 * @method PressureUnit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PressureUnitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PressureUnit::class);
    }

    // /**
    //  * @return PressureUnit[] Returns an array of PressureUnit objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PressureUnit
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
