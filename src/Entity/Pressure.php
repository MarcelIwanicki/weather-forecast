<?php

namespace App\Entity;

use App\Repository\PressureRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PressureRepository::class)
 * @ORM\Table(name="pressure")
 */
class Pressure
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity=PressureUnit::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $pressureUnit;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(float $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getPressureUnit(): ?PressureUnit
    {
        return $this->pressureUnit;
    }

    public function setPressureUnit(?PressureUnit $pressureUnit): self
    {
        $this->pressureUnit = $pressureUnit;

        return $this;
    }
}
