<?php

namespace App\Entity;

use App\Repository\CityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CityRepository::class)
 * @ORM\Table(name="city")
 */
class City
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $latitude;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $longitude;

    /**
     * @ORM\OneToMany(targetEntity=WeatherStatus::class, mappedBy="city")
     */
    private $weatherStatuses;

    public function __construct()
    {
        $this->weatherStatuses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(?float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(?float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return Collection|WeatherStatus[]
     */
    public function getWeatherStatuses(): Collection
    {
        return $this->weatherStatuses;
    }

    public function addWeatherStatus(WeatherStatus $weatherStatus): self
    {
        if (!$this->weatherStatuses->contains($weatherStatus)) {
            $this->weatherStatuses[] = $weatherStatus;
            $weatherStatus->setCity($this);
        }

        return $this;
    }

    public function removeWeatherStatus(WeatherStatus $weatherStatus): self
    {
        if ($this->weatherStatuses->removeElement($weatherStatus)) {
            // set the owning side to null (unless already changed)
            if ($weatherStatus->getCity() === $this) {
                $weatherStatus->setCity(null);
            }
        }

        return $this;
    }
}
