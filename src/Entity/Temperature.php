<?php

namespace App\Entity;

use App\Repository\TemperatureRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TemperatureRepository::class)
 * @ORM\Table(name="temperature")
 */
class Temperature
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $value;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $feels_like_value;

    /**
     * @ORM\ManyToOne(targetEntity=TemperatureUnit::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $unit;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(float $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getFeelsLikeValue(): ?float
    {
        return $this->feels_like_value;
    }

    public function setFeelsLikeValue(?float $feels_like_value): self
    {
        $this->feels_like_value = $feels_like_value;

        return $this;
    }

    public function getUnit(): ?TemperatureUnit
    {
        return $this->unit;
    }

    public function setUnit(?TemperatureUnit $unit): self
    {
        $this->unit = $unit;

        return $this;
    }
}
