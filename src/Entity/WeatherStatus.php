<?php

namespace App\Entity;

use App\Repository\WeatherStatusRepository;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;

/**
 * @ORM\Entity(repositoryClass=WeatherStatusRepository::class)
 * @ORM\Table(name="weather_status")
 */
class WeatherStatus
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $icon_name;

    /**
     * @ORM\ManyToOne(targetEntity=City::class, inversedBy="weatherStatuses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $city;

    /**
     * @ORM\OneToOne(targetEntity=Temperature::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $temperature;

    /**
     * @ORM\ManyToOne(targetEntity=WindDirection::class)
     */
    private $windDirection;

    /**
     * @ORM\OneToOne(targetEntity=Pressure::class, cascade={"persist", "remove"})
     */
    private $pressure;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getIconName(): ?string
    {
        return $this->icon_name;
    }

    public function setIconName(?string $icon_name): self
    {
        $this->icon_name = $icon_name;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getTemperature(): ?Temperature
    {
        return $this->temperature;
    }

    public function setTemperature(Temperature $temperature): self
    {
        $this->temperature = $temperature;

        return $this;
    }

    public function getWindDirection(): ?WindDirection
    {
        return $this->windDirection;
    }

    public function setWindDirection(?WindDirection $windDirection): self
    {
        $this->windDirection = $windDirection;

        return $this;
    }

    public function getPressure(): ?Pressure
    {
        return $this->pressure;
    }

    public function setPressure(?Pressure $pressure): self
    {
        $this->pressure = $pressure;

        return $this;
    }

    #[Pure] public function __toString(): string
    {
        return $this->getCity()->getName() . ", " . $this->getTemperature()->getValue() . " " . $this->getTemperature()->getUnit()->getUnit();
    }


}
