<?php

namespace App\Form;

use App\Entity\Pressure;
use App\Entity\WeatherStatus;
use App\Entity\City;
use App\Entity\Temperature;
use App\Entity\WindDirection;
use SebastianBergmann\CodeCoverage\Report\Text;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class WeatherStatusType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('date')
            ->add('icon_name')
            ->add('city', EntityType::class, [
                'class' => City::class,
                'choice_label' => 'name'
            ])
            ->add('temperature', TemperatureType::class)
            ->add('windDirection', EntityType::class, [
                'class' => WindDirection::class,
                'choice_label' => 'direction'
            ])
            ->add('pressure', PressureType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => WeatherStatus::class,
        ]);
    }
}
