<?php

namespace App\Service;

use App\Repository\CityRepository;
use App\Repository\WeatherStatusRepository;

class WeatherUtil
{
    private CityRepository $cityRepository;
    private WeatherStatusRepository $weatherStatusRepository;

    /**
     * WeatherUtil constructor.
     * @param $cityRepository
     * @param $weatherStatusRepository
     */
    public function __construct(CityRepository $cityRepository, WeatherStatusRepository $weatherStatusRepository)
    {
        $this->cityRepository = $cityRepository;
        $this->weatherStatusRepository = $weatherStatusRepository;
    }


    public function getWeatherStatusByCityName($cityName)
    {
        $city = $this->cityRepository->findByName($cityName)[0];
        return $this->weatherStatusRepository->findByCity($city);
    }

    public function getCityByName($name)
    {
        return $this->cityRepository->findByName($name)[0];
    }

    public function getWeatherStatusByLocation($location)
    {
        $latitude = explode(',', $location)[0];
        $longitude = explode(',', $location)[1];

        $city = $this->cityRepository->findByLocation($latitude, $longitude)[0];
        return $this->weatherStatusRepository->findByCity($city);
    }

    public function getCityByLocation($location)
    {
        $latitude = explode(',', $location)[0];
        $longitude = explode(',', $location)[1];

        return $this->cityRepository->findByLocation($latitude, $longitude)[0];
    }

    public function getWeatherStatusById($id)
    {
        return $this->weatherStatusRepository->find($id);
    }
}